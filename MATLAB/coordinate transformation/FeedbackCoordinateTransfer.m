function [X,Y,X_dot,Y_dot,X_ddot,Y_ddot] = FeedbackCoordinateTransfer(th1,th2,th1_dot,th2_dot,th1_ddot,th2_ddot)
%Coordinate transfer function for feedback loop
%  Recieves all the theta values from the forward kinematics
Y = sin(th1)*r1 + sin(th2)*r2;
X = cos(th1)*r1 + cos(th2)*r2;
Y_dot = cos(th1)*r1*th1_dot + cos(th2)*r2*th2_dot;
X_dot = -sin(th1)*r1*th1_dot - sin(th2)*r2*th2_dot;
Y_ddot = -sin(th1)*r1*(th1_dot^2) + cos(th1)*r1*th1_ddot - sin(th2)*r2*(th2_dot^2) + cos(th2)*r2*th2_ddot;
X_ddot = -cos(th1)*r1*(th1_dot^2) - sin(th1)*r1*th1_ddot - cos(th2)*r2*(th2_dot^2) - sin(th2)*r2*th2_ddot;
end

