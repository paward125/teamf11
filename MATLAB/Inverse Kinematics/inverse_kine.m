function [theta_2 , theta_3] = inverse_kine(theta_2g , theta_3g , r_1 , theta_1)

% inverse_kine Inverse Kinematic Function
%   Takes two input guesses and returns the actual values values based off
%   of reducing error. Theta_2g and Theta_3g are the last calculated values
%   of theta 2 and theta 3 respectively that we will use as a point to
%   start iterating from. r_1 is the known magnitude vector 1 and theta_1
%   is the angle of that vector.

%% Inverse Kinematics for In Contact

% Knowns
th_1 = theta_1; % Theta One
s1 = sind(th_1); % Sin theta 1
c1 = cosd(th_1); % Cos theta 1
r1 = r_1; %  Hip to foot length
r2 = 1; % Shank length
r3 = 2; % Thigh length

% Initial Guesses

th_2 = theta_2g*pi/180; % Theta 2
th_3 = theta_3g*pi/180; % Theta 3


% Our reasonable tolerance for position
epsilon = 5e-5;

% Setting a base epsilon to insure that code is read
epsilon1 = 1;
epsilon2 = 1;

% Setting a run-out timer
runs = 0;



%% Looping to converge on answer

while (epsilon1>= epsilon || epsilon2 >= epsilon && runs <= 10)
    
    runs = runs +1;
    
    % Defining Sin and Cos for our angles
    s2 = sin(th_2);
    s3 = sin(th_3);
    c2 = cos(th_2);
    c3 = cos(th_3);
    
    %Equations of Motion based off our angles
    x = s1*r1 + s2*r2 - s3*r3;
    y = c1*r1 + c2*r2 - c3*r3;
    
    %Partial derivatives
    dx_dth2 =c2*r2;
    dx_dth3 =-c3*r3;
    dy_dth2 =-s2*r2;
    dy_dth3 =s3*r3;
    
    %Defining Jacobian
    Jac = [dx_dth2 , dx_dth3 ; dy_dth2 , dy_dth3];
    
    %Setting old thetas and storing as "n"
    th_2n = th_2;
    th_3n = th_3;
    
    %Reguessing 
    xn_yn = [th_2;th_3] - ((Jac)\[x;y]) ;
    th_2 = xn_yn(1);
    th_3 = xn_yn(2);
    
    %Subtracting to refine our guesses
    epsilon1 = abs(th_2 - th_2n);
    epsilon2 = abs(th_3 - th_3n);
    


end

    theta_2 = th_2 * 180/pi;
    theta_3 = th_3 * 180/pi;


end