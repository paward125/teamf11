%% Lagrangian Model of SDOF Leg
%
% *Brief:*
% 
% This script works in conjunction with simulink file "Lagrangian_sim" to
% simulated the motion of a SDOF leg. The leg is attached to a test stand 
% which restricts the leg to only vertical motion.
%
% *Contents:*
% 
% There is a Simple Model section and a Constrained Model section. Both
% sections model the SDOF leg. The Simple Model does not include the
% no-slip-condition at the foot but the Constrained Model does. The
% no-slip-condition allows use to model the leg as fixed laterally at the
% ground as well as at the hip. The results of each section demonstrate the
% affect of including this condition. Each section defines its intitial
% conditions for angle and angular velocity of theta1 and theta2
%
% Additionally, there is a Setup section and a Constant section. The Setup
% section simply calls the simulink file that is used and the Constant
% section defines the properties of the leg. 
%
% *Notes:*
% 
% - The Lagrangian used to model the system is in the OneNote file, follow
%   path < Robotic Locomotion/Analysis/Lagrangian >
% - Leg properties are defined in Constants section and can be modified to
%   reflect changes in leg structure
% - Torque inputs are modeled in Simulink

%% Constants 
% Define properties of the leg and initial conditions
tau1 = 15; % (positive)

tau2 = -20; % (negative)

% Length of thigh 
T = .5; % [m]
 
% Length of shank
S = .4; % [m]

% mass of motor at hip
m1 = 5; % [kg]

% mass of motor at knee
m2 = 0; % [kg]

%% Run Unconstrained Model
% Does not include no-slip-condition

% Define un-constrained IC
% Initial values for theta_1 and theta_2
% t = [ theta_1; theta_2]

t1 = pi/9;
t2 = t1+pi/9;
t = [t1;t2];% [rad]

% Initial values for thetaDot_1 and thetaDot_2
% t = [ theta_1_dot; theta_2_dot]

tDot = [0;0]; % [rad/s]
   
% Connect the simulink file to this script
out = sim("Lagrangian_sim_unconstrained.slx");

%% Process Data from Unconstrained Model

% Store data output
% NOTE: If data1 does not collect data, run the simulink file and then
% re-run this section of code
data = out.unconstrained;

% Animation for verification
hf5 = figure(5);
hf5.Visible = 'on';
hf5.WindowState = 'maximized';

for i = 1:length(out.tout-2)
%     pos = [foot position, knee position, hip position]
    x_pos = [T*cos(data(i,1))-S*cos(data(i,2)), T*cos(data(i,1)), 0];
    y_pos = [-T*sin(data(i,1))-S*sin(data(i,2)), -T*sin(data(i,1)), T*sin(data(i,1))+S*sin(data(i,2))];
    
    plot(x_pos, y_pos);
    hold on
    plot(T*cos(data(i,1)),-T*sin(data(i,1)),'r')
    hold off
    grid on
    yline(0);
    xlim([-5 5]);
    ylim([-5 5]);
    drawnow;
    frame = getframe(hf5); % this is for saving the gif
    im{i} = frame2im(frame); % this is for saving the gif
end

%% Create a gif to show motion of leg

filename = 'unconstrained.gif';
for i = 1:length(out.tout-2)
    [A, map] = rgb2ind(im{i},256);
    if i == 1
        imwrite(A, map, filename, 'gif', 'LoopCount', Inf, 'DelayTime', 0.1);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode','append', 'DelayTime',0.1);
    end
end



%% Constants
% Define properties of the leg and initial conditions
tic

tau1 = 15; % (positive)

tau2 = -20; % (negative)

% Length of thigh 
T = .5; % [m]
 
% Length of shank
S = .4; % [m]

% mass of motor at hip
m1 = 5; % [kg]

% mass of motor at knee
m2 = 0; % [kg]
%% No-Slip Model
% % Includes no-slip-condition
% 
% % Initial values for theta_1 and theta_2
t1 = deg2rad(-30);
t2 = deg2rad(-135);
t = [t1;t2];% [rad]
% 
% % Initial values for thetaDot_1 and thetaDot_2
% % t = [ theta_1_dot; theta_2_dot; lambda_dot]
% 
tDot = [0;0]; % [rad/s]

time = 0.4;
   
% % Connect the simulink file to this script
% run("Lagrangian_sim_noslip.slx");
% 
% % Store data output
out = sim("Lagrangian_sim_noslip.slx");
toc
%% Process Data from No-Slip Model

% Store data output
% NOTE: If data1 does not collect data, run the simulink file and then
% re-run this section of code
data = out.noslip;

% Animation for verification
hf5 = figure(5);
hf5.Visible = 'on';
hf5.WindowState = 'maximized';

for i = 1:length(out.tout)
%     pos = [foot position, knee position, hip position]
    x_pos = [0, -S*cos(data(i,2)), -S*cos(data(i,2))-T*cos(data(i,1))];
    y_pos = [0, -S*sin(data(i,2)), -T*sin(data(i,1))-S*sin(data(i,2))];
    
    plot(x_pos, y_pos);
    hold on
    plot(-S*cos(data(1:i,2))-T*cos(data(1:i,1)),-T*sin(data(1:i,1))-S*sin(data(1:i,2)),'r');
    hold off
    grid on
    yline(0);
    xlim([-1 1]);
    ylim([-1 1]);
    drawnow;
    frame = getframe(hf5); % this is for saving the gif
    im{i} = frame2im(frame); % this is for saving the gif
end

%% Create a gif to show motion of leg

filename = 'noslip.gif';
for i = 1:length(out.tout-2)
    [A, map] = rgb2ind(im{i},256);
    if i == 1
        imwrite(A, map, filename, 'gif', 'LoopCount', Inf, 'DelayTime', 0.1);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode','append', 'DelayTime',0.1);
    end
end
